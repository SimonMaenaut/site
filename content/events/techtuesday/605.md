---
techtuenr: "605"
startdate: 2021-11-30
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue605
series: TechTuesday
title: TechTuesday 605
linktitle: "TechTue 605"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
