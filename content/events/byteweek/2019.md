---
startdate:  2019-01-25
starttime: "19:00"
enddate: 2019-02-02
#endtime: "05h"
allday: true
linktitle: "ByteWeek 2019"
title: "Byteweek 2019"
location: "HSBXL"
eventtype: "Eight bits of awesomeness"
price: ""
image: "byteweek.png"
series: "byteweek2019"
start: "true"
---

The week before Fosdem conference, HSBXL compiles ByteWeek.  
Eight 'Bitdays' of hackatons, workshops and talks.  
We end our week with the notorious [Bytenight](/bytenight)!

# Friday January 25th
{{< events series="byteweek2019" when="2019-01-25" showtime="true" showallday="true" >}}
# Saturday January 26th
{{< events series="byteweek2019" when="2019-01-26" showtime="true" showallday="true" >}}
# Sunday January 27th
{{< events series="byteweek2019" when="2019-01-27" showtime="true" showallday="true" >}}
# Monday January 28th
{{< events series="byteweek2019" when="2019-01-28" showtime="true" showallday="true" >}}
# Tuesday January 29th
{{< events series="byteweek2019" when="2019-01-29" showtime="true" showallday="true" >}}
# Wednesday January 30th
{{< events series="byteweek2019" when="2019-01-30" showtime="true" showallday="true" >}}
# Thursday January 31th
{{< events series="byteweek2019" when="2019-01-31" showtime="true" showallday="true" >}}
# Friday February 1st
{{< events series="byteweek2019" when="2019-02-01" showtime="true" showallday="true" >}}
# Saturday February 2nd
{{< events series="byteweek2019" when="2019-02-02" showtime="true" showallday="true" >}}
# Call for participation
If you want to have a talk, workshop or hackaton,  
send a mail to **contact@hsbxl.be** with your proposal.

# Food
We are serving fresh soup every day.

# Aftermath
Byteweek was a **BLAST**!  
Financialy we also did **very** well.  

## Costs
Costs Byteweek related are **€1551,31**.

- 18 fries/soup packages
- 95,99 deep fryer / fries / pizzas
- 55,50 pellets
- 13,5 pellets
- 21,7 pellets
- 190,56 colruyt
- 23,30 pellets
- 174,2 colruyt
- 440,56 beer
- 518 beer
- 26 broken chairs VGC

## Revenue
Byteweek brought us **€2851,5**.

## Profit
Which means Byteweek added **€1274,19** to our warchest.
