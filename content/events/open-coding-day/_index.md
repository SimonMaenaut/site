---
linktitle: "Open Coding Day Events"
title: "Open Coding Day Events"
location: "HSBXL"
eventtype: "co-working event"
price: "Free"
aliases: [/open-coding-day/]
series: "Open Coding Day"
start: "true"
---

Every Friday is Open Coding Friday,  
a day of coding collaboration  
for both members and visitors at HSBXL.

## Upcoming Open Coding Day Events

{{< events when="upcoming" series="Open Coding Day" >}}

## Past Open Coding Day Events

{{< events when="past" series="Open Coding Day" >}}
