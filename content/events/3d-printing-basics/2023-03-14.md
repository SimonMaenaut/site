---
startdate:  2023-03-14
starttime: "19:00"
endtime: "20:00"
linktitle: "Using FreeCAD to create for the 3D printer"
title: "3D printing: using FreeCAD"
price: "free donation"
image: "techtuesday.jpg"
eventtype: "Workshop"
location: "HSBXL"
---

In this mini-workshop, we'll design and create a knob for the hot plates in the kitchen - using [FreeCAD](https://www.freecad.org/downloads.php).

**Requirements:**
* having FreeCAD installed on your computer
* a regular mouse is interesting


**We will be looking at**
* the basics of FreeCAD (understanding the interface)
  * Uh? What's this crappy interface?
  * Understanding the logic
  * [Part or Part Design](https://wiki.freecad.org/Part_and_PartDesign/en) workbench?
* if you don't know the answer: where to find help
  * [The Wiki](https://wiki.freecad.org/)
  * The Community [Forum](https://forum.freecad.org/) and [IRC Chat](https://web.libera.chat/#freecad)
  * Stay informed over [Mastodon](https://fosstodon.org/@FreeCAD)
* Using the [part design workbench](https://wiki.freecad.org/PartDesign_Workbench)
  * Working with [sketches and constraints](https://wiki.freecad.org/Sketcher_Workbench)
  * [pads](https://wiki.freecad.org/PartDesign_Pad) and [pockets](https://wiki.freecad.org/PartDesign_Pocket)
  * combining elements
  * linear and radial patterns
* keep the design process in mind: we're 3D printing...

Basic notes on 3D printing in documentation:
https://cloud.hsbxl.be/index.php/s/QQ5Et4HpM5r7NSW