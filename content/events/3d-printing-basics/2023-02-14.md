---
startdate:  2023-02-14
starttime: "19:00"
endtime: "20:00"
linktitle: "3D printing basics"
title: "3D printing: The basics"
price: "free donation"
image: "techtuesday.jpg"
eventtype: "Workshop"
location: "HSBXL"
---

## DATE CORRECTED: workshop is on TUESDAY

Basic concepts of 3D printing with an FDM printer

We will be looking at:
* a basic understanding of how 3D printers like the Lulzbot and Ender 3 Pro work
* the basic steps of creation: design file, save as an STL file, slice to a gcode file
* pick an existing design from the web
* slicing it using Cura

Notes on 3D printing in HSBXL documentation:
https://cloud.hsbxl.be/index.php/s/QQ5Et4HpM5r7NSW